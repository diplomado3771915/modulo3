import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: "varchar", length: 30 })
  name: string;

  @Column({ type: "varchar", length: 15 })
  username: string;

  @Column({ type: "varchar", length: 50 })
  email: string;

  @Column({ type: "varchar" })
  password: string;

  constructor(partial?: Partial<User>) {
    Object.assign(this, partial);
  }
}
