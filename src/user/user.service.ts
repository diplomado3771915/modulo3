import {
  ConflictException,
  Injectable,
  UnauthorizedException,
} from "@nestjs/common";
import { CreateUserDto } from "./dto/create-user.dto";
import { UpdateUserDto } from "./dto/update-user.dto";
import { UserRepository } from "./user.repository";

@Injectable()
export class UserService {
  constructor(private readonly userRepository: UserRepository) {}

  async create(createUserDto: CreateUserDto) {
    const usuarioExistente = await this.userRepository.findByUsername(
      createUserDto.username
    );
    if (usuarioExistente) {
      throw new ConflictException("El name de usuario ya está en uso");
    }
    return this.userRepository.create(createUserDto);
  }

  async validate(username: string, password: string) {
    const usuario = await this.userRepository.findByUsername(username);
    if (
      usuario &&
      usuario.password === password &&
      usuario.username === username
    ) {
      return usuario;
    }
    throw new UnauthorizedException("Credenciales inválidas");
  }

  findOne(id: number) {
    return this.userRepository.findById(id);
  }

  findAll() {
    return this.userRepository.list();
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    const usuario = this.userRepository.findById(id);
    if (!usuario) {
      throw new Error(`Usuario con id ${id} no encontrado`);
    }
    return this.userRepository.update(id, updateUserDto);
  }

  delete(id: number) {
    return this.userRepository.delete(id);
  }
}
