import { ApiProperty } from "@nestjs/swagger";
import {
  IsEmail,
  IsNotEmpty,
  IsString,
  MinLength,
  IsAlphanumeric,
} from "class-validator";

export class CreateUserDto {
  @ApiProperty({
    description: "User name",
    example: "Juan",
    required: true,
  })
  @IsString()
  @IsNotEmpty()
  @MinLength(5, { message: "Name must have at least 5 characters" })
  name: string;

  @ApiProperty({
    description: "User nickname",
    example: "juan123",
    required: true,
  })
  @IsString()
  @IsNotEmpty()
  @MinLength(3, {
    message: "Username must have at least 3 characters",
  })
  @IsAlphanumeric(null, {
    message: "Username must have only letters and numbers",
  })
  username: string;

  @ApiProperty({
    description: "User email",
    example: "test@test.com",
    required: true,
  })
  @IsString()
  @IsNotEmpty()
  @IsEmail(null, { message: "Invalid email" })
  email: string;

  @ApiProperty({
    description: "User password",
    example: "123456",
    required: true,
  })
  @IsString()
  @IsNotEmpty()
  password: string;
}
