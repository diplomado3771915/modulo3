import { PartialType } from "@nestjs/mapped-types";
import { CreateUserDto } from "./create-user.dto";
import {
  IsAlphanumeric,
  IsEmail,
  IsNotEmpty,
  IsString,
  MinLength,
} from "class-validator";

export class UpdateUserDto extends PartialType(CreateUserDto) {
  @IsString()
  @IsNotEmpty()
  @MinLength(5, { message: "Name must have at least 5 characters" })
  name: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(3, {
    message: "Username must have at least 3 characters",
  })
  @IsAlphanumeric(null, {
    message: "Username must have only letters and numbers",
  })
  username: string;

  @IsString()
  @IsNotEmpty()
  @IsEmail(null, { message: "Invalid email" })
  email: string;

  @IsString()
  @IsNotEmpty()
  password: string;
}
