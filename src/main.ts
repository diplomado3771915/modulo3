import { NestFactory } from "@nestjs/core";
import { SwaggerModule, DocumentBuilder } from "@nestjs/swagger";
import { AppModule } from "./app.module";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const options = new DocumentBuilder()
    .setTitle("API")
    .setDescription("API description")
    .setVersion("1.0")
    .addServer("http://localhost:3000")
    .addTag("api")
    .addBearerAuth({
      type: "http",
      scheme: "bearer",
      description: "Default JWT Authorization",
      bearerFormat: "JWT",
    })
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup("api-docs", app, document);
  await app.listen(3000);
}
bootstrap();
