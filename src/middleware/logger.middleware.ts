import { Injectable, Logger } from "@nestjs/common";

@Injectable()
export class LoggerMiddleware {
  private readonly logger = new Logger(LoggerMiddleware.name);
  use(req: Request, res: Response, next: (error?: any) => void) {
    this.logger.log("Request...", req.method, req.url);
    this.logger.log("Reponse...", res);
    next();
  }
}
